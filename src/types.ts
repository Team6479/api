import 'express-session'

declare module 'express-session' {
    interface SessionData {
        token?: DiscordTokenResponse,
        member?: GuildMember
    }
}

export type DiscordErrorResponse = {
    error: string
    error_description: string
}

export type DiscordTokenResponse = {
    token_type: string
    access_token: string
    expires_in: number
    refresh_token: string
    scope: string
}

export type User = {
    id: string
    username: string
    discriminator: string
    global_name?: string
    avatar?: string
    banner?: string
    banner_color?: string
    accent_color?: number
}

export type GuildMember = {
    user: User,
    nick?: string,
    avatar?: string,
    roles: string[]
}