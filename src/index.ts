import './types'
import 'dotenv/config'
import cors from 'cors'
import express from 'express'
import session from 'express-session'
import loginRouter from './login'
import scoutingRouter from './scouting'

const app = express()

app.use(express.json())

app.use(session({
    secret: 'secret thing is very secret',
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: false // TODO: Change 
    } // TODO: switch to postgres store instead of unsafe memory store
}))

app.use(cors({
    origin: '*', // TODO: change when uploaded to server
    optionsSuccessStatus: 200
}))

app.get('/', async (req, res) => {
    // api docs
    res.json({})
})

app.use('/login', loginRouter)
app.use('/scouting', scoutingRouter)

app.listen(8080, () => console.log('Server listening on http://localhost:8080'))