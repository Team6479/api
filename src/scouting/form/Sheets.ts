import { google, sheets_v4 } from "googleapis"
import * as gaxios from 'gaxios'

gaxios.instance.defaults.validateStatus = () => true

const auth = new google.auth.JWT({
	email: process.env.SERVICE_ACCOUNT_EMAIL,
	key: process.env.SERVICE_ACCOUNT_PRIVATE_KEY,
	scopes: ['https://www.googleapis.com/auth/spreadsheets']
})

export const client = google.sheets({
	version: 'v4',
	auth,
})

export class Spreadsheet {
  private spreadsheetId: string
  private sheets: sheets_v4.Schema$Sheet[] | undefined

  constructor(spreadsheetId: string) {
    this.spreadsheetId = spreadsheetId
  }

  async fetch(ranges: string[] | undefined = undefined) {
    const res = await client.spreadsheets.get({
      includeGridData: true,
      ranges,
      spreadsheetId: this.spreadsheetId,
    })
    this.sheets = res.data.sheets
    return res
  }

  async create(title: string) {
    if(!title) return false
    const res = await client.spreadsheets.batchUpdate({
      spreadsheetId: this.spreadsheetId,
      requestBody: {
        includeSpreadsheetInResponse: true,
        requests: [{
          addSheet: {
            properties: {
              title: title
            }
          }
        }]
      }
    })
    this.sheets = res.data.updatedSpreadsheet?.sheets
    return res
  }

  async sheetExists(title: string | undefined, includeData: boolean = false) {
    if(!this.sheets) await this.fetch()
    if(title == undefined) return false
    for(const sheet of this.sheets || []) {
      if(sheet.properties?.title == title) {
        return includeData ? sheet : true
      }
    }
    return includeData ? null : false
  }

  async update(title: string | undefined, rowIndex: number, row: any[]) {
    if(title && !(await this.sheetExists(title))) return false
    return await client.spreadsheets.values.update({
      spreadsheetId: this.spreadsheetId,
      range: `${title ? "'" + title + "'!" : ''}${(rowIndex + 1) + ':' + (rowIndex + 1)}`,
      valueInputOption: 'RAW',
      requestBody: {
        majorDimension: 'ROWS',
        values: [row]
      }
    })
  }

  async appendRows(title: string | undefined, values: any[][], rowIndex: number = 0) {
    if(title && !(await this.sheetExists(title))) return false
    return client.spreadsheets.values.append({
      spreadsheetId: this.spreadsheetId,
      range: `${title ? "'" + title + "'!" : ''}${rowIndex+1}:${rowIndex+1+values.length}`,
      insertDataOption: 'INSERT_ROWS',
      valueInputOption: 'RAW',
      requestBody: {
        majorDimension: 'ROWS',
        values: values
      },
    })
  }
}