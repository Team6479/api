import { Router } from "express";
import formRouter from './form'
import teamsRouter from './teams'
import teamRouter from './team'

const router = Router()

router.use('/form', formRouter)
router.use('/teams', teamsRouter)
router.use('/team', teamRouter)

export default router