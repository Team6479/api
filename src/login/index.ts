import axios from "axios";
import { Router } from "express";
import { DiscordErrorResponse, DiscordTokenResponse, GuildMember } from 'src/types'

const oauthInstance = axios.create({
    baseURL: 'https://discord.com/api/oauth2/',
    validateStatus: () => true,
})
const discordInstance = axios.create({
    baseURL: 'https://discord.com/api/v10/',
    validateStatus: () => true
})

const clientId = process.env.CLIENT_ID
const clientSecret = process.env.CLIENT_SECRET
const redirectUri = process.env.REDIRECT_URI
// https://discord.com/api/oauth2/authorize?client_id=1058984104739221524&response_type=code&redirect_uri=http://localhost:8080/login&scope=guilds+guilds.members.read+identify

const router = Router()

router.get('/', async (req, res) => {
    // Check if already logged in
    if (req.session.token) {
        const member = req.session.member
        return res.json({
            success: true,
            data: {
                name: member?.nick || member?.user.global_name || member?.user.username || 'uh oh',
                roles: member?.roles
            }
        })
    }

    // Get the auth code sent by discord
    const { code } = req.query
    if (!code) {
        return res.status(401).json({
            success: false,
            error: 'Not logged in'
        })
    }

    // Get token from discord
    const tokenRes = await oauthInstance.post<DiscordTokenResponse>('token', {
        grant_type: 'authorization_code',
        code,
        redirect_uri: redirectUri,
        client_id: clientId,
        client_secret: clientSecret
    }, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })

    if (tokenRes.status !== 200) {
        console.error(tokenRes.data)
        const error = tokenRes.data as any as DiscordErrorResponse
        return res.json({
            success: false,
            error: error.error_description || 'Internal server error, report to programming'
        })
    }
    const scopes = tokenRes.data.scope.split(' ')
    if (!['identify', 'guilds', 'guilds.members.read'].every(scope => scopes.includes(scope))) {
        return res.json({
            success: false,
            error: 'Not enough scopes provided'
        })
    }
    req.session.token = tokenRes.data

    // Get member and role data
    const memberRes = await discordInstance.get<GuildMember>('users/@me/guilds/474685571088908288/member', {
        headers: {
            Authorization: `${req.session.token.token_type} ${req.session.token.access_token}`
        }
    })
    if (memberRes.status !== 200) {
        console.error(memberRes.data)
        return res.json({
            success: false,
            error: 'uh oh'
        })
    }
    const member = req.session.member = memberRes.data
    
    return res.json({
        success: true,
        data: {
            name: member.nick || member.user.global_name || member.user.username || 'uh oh',
            roles: member.roles
        }
    })
})

export default router